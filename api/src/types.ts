export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace Users {
    export interface Model {
        id: number;
        username: string;
        password: string;
        fullName: string;
        image: string;
        phone: number;
        state: boolean;
        createdAt: string;
        updatedAt: string;
    }

    export interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }

    export namespace Create {
        export interface Request {
            username: string;
            password: string;
            fullName: string;
            phone: number;
            image: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {
        export interface Request {
            username: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Update {
        export interface Request {
            username: string;
            password?: string;
            fullName?: string;
            phone?: number;
            image?: string;
            state?: boolean;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace View {
        export interface Request {
            limit?: number;
            offset?: number;
            state?: boolean;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }

    export namespace FindOne {
        export interface Request {
            id?: number;
            username?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Count {
        export interface Request {
            fullName?: string;
            state?: boolean;
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
}

export namespace Products {
    export interface Model {
        id: number;
        name: string;
        mark: string;
        year: string;
        image: string;
        bodega: string;
        state: boolean;
        lote: number;
        code: string;
        createdAt: string;
        updatedAt: string;
    }

    export interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }

    export namespace Create {
        export interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            state?: boolean;
            lote?: number;
            code?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {
        export interface Request {
            ids?: number[];
            marks?: string[];
            year?: string;
            bodegas?: string[];
            state?: boolean;
            lotes?: number[];
            codes?: string[];
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace Update {
        export interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            bodega?: string;
            state?: boolean;
            lote?: number;
            code?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace View {
        export interface Request {
            limit?: number;
            offset?: number;
            marks?: string[];
            year?: string;
            bodegas?: string[];
            state?: boolean;
            lotes?: number[];
            codes?: string[];
        }

        export interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }

    export namespace FindOne {
        export interface Request {
            id: number;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Count {
        export interface Request {
            name?: string;
            mark?: string;
            year?: string;
            bodega?: string;
            state?: boolean;
            lote?: number;
            code?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
}

export namespace Shop {
    export interface Model {
        id: number;
        user: number;
        product: number;
        createdAt: string;
        updatedAt: string;
    }

    export interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }

    export namespace Create {
        export interface Request {
            user: number;
            product: number;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {
        export interface Request {
            users?: number[];
            products?: number[];
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace View {
        export interface Request {
            limit?: number;
            offset?: number;
            users?: string[];
            products?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
}
