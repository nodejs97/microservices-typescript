import { io } from 'socket.io-client';

const socket = io('http://localhost');

async function main() {
    try {
        setTimeout(() => console.log(socket.id), 1500);

        socket.on('res:users:view', ({ statusCode, data, message }) => {
            console.log('res:users:view', { statusCode, data, message });
        });

        socket.on('connect_error', (error) => {
            console.log(error.message);
        });

        setTimeout(() => {
            socket.emit('req:users:view', {});
        }, 300);
    } catch (error) {
        console.log(error);
    }
}

main();
