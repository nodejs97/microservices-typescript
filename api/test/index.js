const api = require('../dist');

const redis = {
    local: {
        host: 'localhost',
        port: 6379,
        password: undefined
    }
};

const environment = redis.local;

const users = [
    {
        fullName: 'John Doe',
        username: 'johndoe',
        password: '123456',
        phone: '1234567890',
        image: 'https://picsum.photos/200'
    },
    {
        fullName: 'Jane Doe',
        username: 'janedoe',
        password: '123456',
        phone: '1234567890',
        image: 'https://picsum.photos/300'
    }
];

async function create(user) {
    try {
        const result = await api.Create(user, environment);

        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

async function del(username) {
    try {
        const result = await api.Delete({ username }, environment);

        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

async function update(params) {
    try {
        const result = await api.Update(params, environment);

        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

async function findOne(params) {
    try {
        const result = await api.FindOne(params, environment);

        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

async function view(params) {
    try {
        const result = await api.View(params, environment);

        console.log(result.data.data);
    } catch (error) {
        console.log(error);
    }
}

async function count(params) {
    try {
        const result = await api.Count(params, environment);

        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

const main = async () => {
    try {
        await view({});
    } catch (error) {
        console.log(error);
    }
};

main();
