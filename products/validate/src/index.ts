import Joi from 'joi';
import * as T from './types';

export async function Create(params: T.Create.Request): Promise<T.Create.Response> {
    try {
        const schema = Joi.object({
            name: Joi.string().required(),
            mark: Joi.string().required(),
            year: Joi.date(),
            image: Joi.string().uri(),
            bodega: Joi.string(),
            state: Joi.boolean(),
            lote: Joi.number(),
            code: Joi.string()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Detele(params: T.Delete.Request): Promise<T.Delete.Response> {
    try {
        const schema = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            marks: Joi.array().items(Joi.string().required()),
            year: Joi.date(),
            bodegas: Joi.array().items(Joi.string().required()),
            state: Joi.boolean(),
            lotes: Joi.array().items(Joi.number().required()),
            codes: Joi.array().items(Joi.string().required())
        }).or('ids', 'marks', 'years', 'bodegas', 'state', 'lotes', 'codes');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Update(params: T.Update.Request): Promise<T.Update.Response> {
    try {
        const schema = Joi.object({
            id: Joi.number().required(),
            name: Joi.string(),
            mark: Joi.string(),
            year: Joi.date(),
            image: Joi.string().uri(),
            bodega: Joi.string(),
            state: Joi.boolean(),
            lote: Joi.number(),
            code: Joi.string()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function View(params: T.View.Request): Promise<T.View.Response> {
    try {
        const schema = Joi.object({
            limit: Joi.number(),
            offset: Joi.number(),
            marks: Joi.array().items(Joi.string()),
            year: Joi.date(),
            bodegas: Joi.array().items(Joi.string()),
            state: Joi.boolean(),
            lotes: Joi.array().items(Joi.number()),
            codes: Joi.array().items(Joi.string().required())
        }).or('limit', 'offset', 'marks', 'year', 'bodegas', 'state', 'lotes', 'codes');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function FindOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {
    try {
        const schema = Joi.object({
            id: Joi.number().required()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Count(params: T.Count.Request): Promise<T.Count.Response> {
    try {
        const schema = Joi.object({
            name: Joi.string(),
            mark: Joi.string(),
            year: Joi.date(),
            bodega: Joi.string(),
            state: Joi.boolean(),
            lote: Joi.number(),
            code: Joi.string()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
