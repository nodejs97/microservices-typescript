export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace Create {
    export interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Delete {
    export interface Request {
        ids?: number[];
        marks?: string[];
        year?: string;
        bodegas?: string[];
        state?: boolean;
        lotes?: number[];
        codes?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Update {
    export interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}

export namespace View {
    export interface Request {
        limit?: number;
        offset?: number;
        marks?: string[];
        year?: string;
        bodegas?: string[];
        state?: boolean;
        lotes?: number[];
        codes?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}

export namespace FindOne {
    export interface Request {
        id: number;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Count {
    export interface Request {
        name?: string;
        mark?: string;
        year?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
