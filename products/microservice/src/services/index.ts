import * as T from '../types';
import * as Models from '../models';
import { InternalError } from '../settings';
import * as Validation from 'validate-products';

export async function Create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {
    try {
        await Validation.Create(params);

        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service Create', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function Detele(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {
    try {
        await Validation.Detele(params);

        const where: T.Models.Where = {};

        const optionals: T.Models.Attributes[] = ['id', 'mark', 'year', 'bodega', 'lote', 'code'];

        for (let x of optionals.map((v) => v.concat('s'))) {
            if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];
        }

        if (params.state !== undefined) where.state = params.state;

        const { statusCode, data, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data };
    } catch (error) {
        console.log({ step: 'Service Delete', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function Update(params: T.Services.Update.Request): Promise<T.Services.Update.Response> {
    try {
        await Validation.Update(params);

        const where: T.Models.Where = { id: params.id };

        const optionals: T.Models.Attributes[] = ['name', 'mark', 'year', 'image', 'bodega', 'lote', 'state', 'code'];

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== 'success') {
            switch (findOne.statusCode) {
                case 'notFound':
                    return { statusCode: 'ValidationError', message: 'Producto no está registrado' };

                default:
                    return { statusCode: 'error', message: InternalError };
            }
        }

        const { statusCode, data, message } = await Models.update(params, { where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data[1][0] };
    } catch (error) {
        console.log({ step: 'Service Update', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function View(params: T.Services.View.Request): Promise<T.Services.View.Response> {
    try {
        await Validation.View(params);

        const where: T.Models.Where = {};

        var optionals: T.Models.Attributes[] = ['mark', 'bodega', 'lote', 'code'];

        for (let x of optionals.map((v) => v.concat('s'))) {
            if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];
        }

        var optionals: T.Models.Attributes[] = ['year', 'state'];

        if (params.state !== undefined) where.state = params.state;

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service View', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function FindOne(params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response> {
    try {
        await Validation.FindOne(params);

        const where: T.Models.Where = { id: params.id };

        const { statusCode, data, message } = await Models.findOne({ where });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service FindOne', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function Count(params: T.Services.Count.Request): Promise<T.Services.Count.Response> {
    try {
        await Validation.Count(params);

        const where: T.Models.Where = {};

        const optionals: T.Models.Attributes[] = ['name', 'mark', 'year', 'bodega', 'lote', 'state', 'code'];

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.count({ where });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service Count', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}
