import { name, RedisOptsQueue as opts, Actions } from '../settings';
import * as Services from '../services';
import { Publish } from '../controller';
import { Worker } from 'bullmq';
import { Adapters as T } from '../types';

export const Process = async (job: T.Process.Request): Promise<T.Process.Response> => {
    try {
        switch (job.name) {
            case 'create': {
                const { statusCode, data, message } = await Services.Create(job.data);

                return { statusCode, data, message };
            }

            case 'delete': {
                const { statusCode, data, message } = await Services.Detele(job.data);

                return { statusCode, data, message };
            }

            case 'findOne': {
                const { statusCode, data, message } = await Services.FindOne(job.data);

                return { statusCode, data, message };
            }

            case 'update': {
                const { statusCode, data, message } = await Services.Update(job.data);

                return { statusCode, data, message };
            }

            case 'view': {
                const { statusCode, data, message } = await Services.View(job.data);

                return { statusCode, data, message };
            }

            case 'count': {
                const { statusCode, data, message } = await Services.Count(job.data);

                return { statusCode, data, message };
            }

            default: {
                return { statusCode: 'error', message: 'Endpoint not found' };
            }
        }
    } catch (error) {
        await Publish({ channel: Actions.error, instance: JSON.stringify({ step: 'Adapters Process', error }) });
    }
};

export const run = async () => {
    try {
        await Publish({
            channel: Actions.start,
            instance: JSON.stringify({ step: 'Adapters Run', message: `Starting ${name}` })
        });

        const worker = new Worker(`${name}:2`, Process, { connection: opts.redis, concurrency: opts.concurrency });

        worker.on('error', async (error) => {
            await Publish({ channel: Actions.error, instance: JSON.stringify({ step: 'Adapters Run', error }) });
        });
    } catch (error) {
        await Publish({ channel: Actions.error, instance: JSON.stringify({ step: 'Adapters run', error }) });
    }
};
