export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export const endpoint = ['create', 'update', 'delete', 'findOne', 'view', 'count'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
    host: string;
    port: number;
    password: string;
}

export interface Model {
    id?: number;
    name?: string;
    mark?: string;
    year?: string;
    image?: string;
    bodega?: string;
    state?: boolean;
    lote?: number;
    code?: string;
    createdAt?: string;
    updatedAt?: string;
}

export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}

export namespace Create {
    export interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Delete {
    export interface Request {
        ids?: number[];
        marks?: string[];
        year?: string;
        bodegas?: string[];
        state?: boolean;
        lotes?: number[];
        codes?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: number;
        message?: string;
    }
}

export namespace Update {
    export interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}

export namespace View {
    export interface Request {
        limit?: number;
        offset?: number;
        marks?: string[];
        year?: string;
        bodegas?: string[];
        state?: boolean;
        lotes?: number[];
        codes?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }
}

export namespace FindOne {
    export interface Request {
        id: number;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Count {
    export interface Request {
        name?: string;
        mark?: string;
        year?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        code?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: number;
        message?: string;
    }
}
