import express, { Express } from 'express';
import http from 'http';
import { Server } from 'socket.io';
import * as apiUsers from 'api-users';
import * as apiProducts from 'api-products';
import * as apiShop from 'api-shop';
import { redis } from './settings';
import { Create, Delete, FindOne, Count, Update } from '../../products/api/src/index';

const app: Express = express();

const server: http.Server = http.createServer(app);

const io = new Server(server);

server.listen(80, () => {
    console.log('Listening on port 80');

    io.on('connection', (socket) => {
        console.log('New connection', socket.id);

        // Endpoints Users
        socket.on('req:users:view', async (params: apiUsers.Types.View.Request) => {
            try {
                console.log('req:users:view');

                const { statusCode, data, message } = await apiUsers.View(params, redis);

                return io.to(socket.id).emit('res:users:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:users:create', async (params: apiUsers.Types.Create.Request) => {
            try {
                console.log('req:users:create');

                const { statusCode, data, message } = await apiUsers.Create(params, redis);

                return io.to(socket.id).emit('res:users:create', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:users:delete', async (params: apiUsers.Types.Delete.Request) => {
            try {
                console.log('req:users:delete');

                const { statusCode, data, message } = await apiUsers.Delete(params, redis);

                return io.to(socket.id).emit('res:users:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:users:update', async (params: apiUsers.Types.Update.Request) => {
            try {
                console.log('req:users:update');

                const { statusCode, data, message } = await apiUsers.Update(params, redis);

                return io.to(socket.id).emit('res:users:update', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:users:findOne', async (params: apiUsers.Types.FindOne.Request) => {
            try {
                console.log('req:users:findOne');

                const { statusCode, data, message } = await apiUsers.FindOne(params, redis);

                return io.to(socket.id).emit('res:users:findOne', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:users:count', async (params: apiUsers.Types.Count.Request) => {
            try {
                console.log('req:users:count');

                const { statusCode, data, message } = await apiUsers.Count(params, redis);

                return io.to(socket.id).emit('res:users:count', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        // Endpoints Products
        socket.on('req:products:view', async (params: apiProducts.Types.View.Request) => {
            try {
                console.log('req:products:view');

                const { statusCode, data, message } = await apiProducts.View(params, redis);

                return io.to(socket.id).emit('res:products:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:products:create', async (params: apiProducts.Types.Create.Request) => {
            try {
                console.log('req:products:create');

                const { statusCode, data, message } = await apiProducts.Create(params, redis);

                return io.to(socket.id).emit('res:products:create', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:products:delete', async (params: apiProducts.Types.Delete.Request) => {
            try {
                console.log('req:products:delete');

                const { statusCode, data, message } = await apiProducts.Delete(params, redis);

                return io.to(socket.id).emit('res:products:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        // Endpoints Shop
        socket.on('req:shop:view', async (params: apiShop.Types.View.Request) => {
            try {
                console.log('req:shop:view');

                const { statusCode, data, message } = await apiShop.View(params, redis);

                return io.to(socket.id).emit('res:shop:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:shop:create', async (params: apiShop.Types.Create.Request) => {
            try {
                console.log('req:shop:create');

                const { statusCode, data, message } = await apiShop.Create(params, redis);

                return io.to(socket.id).emit('res:shop:create', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:shop:delete', async (params: apiShop.Types.Delete.Request) => {
            try {
                console.log('req:shop:delete');

                const { statusCode, data, message } = await apiShop.Delete(params, redis);

                return io.to(socket.id).emit('res:shop:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });
    });
});
