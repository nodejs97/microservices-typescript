import Joi from 'joi';
import * as T from './types';

export async function Create(params: T.Create.Request): Promise<T.Create.Response> {
    try {
        const schema = Joi.object({
            user: Joi.number().required(),
            product: Joi.number().required()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Detele(params: T.Delete.Request): Promise<T.Delete.Response> {
    try {
        const schema = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            users: Joi.array().items(Joi.number().required()),
            products: Joi.array().items(Joi.number().required())
        }).xor('ids', 'users', 'products');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function View(params: T.View.Request): Promise<T.View.Response> {
    try {
        const schema = Joi.object({
            limit: Joi.number(),
            offset: Joi.number(),
            users: Joi.array().items(Joi.number().required()),
            products: Joi.array().items(Joi.number().required())
        }).or('limit', 'offset', 'users', 'products');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
