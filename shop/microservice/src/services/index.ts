import * as T from '../types';
import * as Models from '../models';
import { InternalError } from '../settings';
import * as Validation from 'validate-shop';
import * as Controller from '../controller';

export async function Create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {
    try {
        await Validation.Create(params);

        // Validación del usuario
        const user = (await Controller.ValidateUser({ user: params.user })).data;

        if (!user.state) {
            throw { statusCode: 'ValidationError', message: 'El usuario no está habilitado para realizar compras' };
        }

        // Validación del producto
        const product = (await Controller.ValidateProduct({ product: params.product })).data;

        if (!product.state) {
            throw { statusCode: 'ValidationError', message: 'El producto no tiene stock para realizar ventas' };
        }

        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service Create', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function Detele(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {
    try {
        await Validation.Detele(params);

        const where: T.Models.Where = {};

        const optionals: T.Models.Attributes[] = ['id', 'user', 'product'];

        for (let x of optionals.map((v) => v.concat('s'))) {
            if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];
        }

        const { statusCode, data, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data };
    } catch (error) {
        console.log({ step: 'Service Delete', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function View(params: T.Services.View.Request): Promise<T.Services.View.Response> {
    try {
        await Validation.View(params);

        const where: T.Models.Where = {};

        var optionals: T.Models.Attributes[] = ['user', 'product'];

        for (let x of optionals.map((v) => v.concat('s'))) {
            if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];
        }

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'Service View', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}
