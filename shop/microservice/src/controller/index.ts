import { InternalError, redisClient, RedisOptsQueue as opts } from '../settings';
import { Controllers as T } from '../types';
import * as apiUser from 'api-users';
import * as apiProduct from 'api-products';

export const Publish = async (props: T.Publish.Request): Promise<T.Publish.Response> => {
    try {
        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return { statusCode: 'success', data: props };
    } catch (error) {
        console.log({ step: 'Controller Publish', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
};

export const ValidateUser = async (props: T.ValidateUser.Request): Promise<T.ValidateUser.Response> => {
    try {
        const { statusCode, data, message } = await apiUser.FindOne({ id: props.user }, opts.redis);

        if (statusCode !== 'success') {
            switch (statusCode) {
                case 'notFound':
                    throw { statusCode: 'ValidationError', message: 'No existe el usuario que validas' };

                default:
                    throw { statusCode, message };
            }
        }

        return { statusCode: 'success', data };
    } catch (error) {
        console.log({ step: 'Controller ValidateUser', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
};

export const ValidateProduct = async (props: T.ValidateProduct.Request): Promise<T.ValidateProduct.Response> => {
    try {
        const { statusCode, data, message } = await apiProduct.FindOne({ id: props.product }, opts.redis);

        if (statusCode !== 'success') {
            switch (statusCode) {
                case 'notFound':
                    throw { statusCode: 'ValidationError', message: 'No existe el producto que validas' };

                default:
                    throw { statusCode, message };
            }
        }

        return { statusCode: 'success', data };
    } catch (error) {
        console.log({ step: 'Controller ValidateUser', error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
};
