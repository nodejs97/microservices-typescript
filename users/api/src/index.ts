import { Queue, QueueEvents, Job, JobsOptions } from 'bullmq';
import * as T from './types';

export const name = 'users';
export const version = 2;
export { T as Types };

function genericQueue<Request, Response>(endpointName: T.Endpoint) {
    return async (props: Request, redis: T.REDIS, opts?: JobsOptions): Promise<Response> => {
        try {
            const queue = new Queue(`${name}:${version}`, { connection: redis });
            const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

            const endpoint: T.Endpoint = endpointName;

            const job = await queue.add(endpoint, props, opts);

            await job.waitUntilFinished(queueEvents);

            const result = await Job.fromId(queue, job.id);

            const { statusCode, data, message } = result.returnvalue;

            return { statusCode, data, message } as Response;
        } catch (error) {
            throw { statusCode: 'error', message: error.toString() };
        }
    };
}

export const Create = genericQueue<T.Create.Request, T.Create.Response>('create');

export const Delete = genericQueue<T.Delete.Request, T.Delete.Response>('delete');

export const Update = genericQueue<T.Update.Request, T.Update.Response>('update');

export const FindOne = genericQueue<T.FindOne.Request, T.FindOne.Response>('findOne');

export const View = genericQueue<T.View.Request, T.View.Response>('view');

export const Count = genericQueue<T.Count.Request, T.Count.Response>('count');
