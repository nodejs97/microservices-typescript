import { SyncDB } from './models';
import { run } from './adapter';
import { redisClient } from './settings';

redisClient.on('error', (error) => console.log('Redis Client Error', error));

export { SyncDB, run };
