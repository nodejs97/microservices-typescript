import Joi from 'joi';
import * as T from './types';

export async function Create(params: T.Create.Request): Promise<T.Create.Response> {
    try {
        const schema = Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
            fullName: Joi.string().required(),
            phone: Joi.number().required(),
            image: Joi.string().uri().required()
        });

        const result = await schema.validateAsync(params);

        if (['admin', 'root', 'su'].includes(params.username)) {
            throw { statusCode: 'error', message: 'Nombre reservado para uso interno' };
        }

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Detele(params: T.Delete.Request): Promise<T.Delete.Response> {
    try {
        const schema = Joi.object({
            username: Joi.string().required()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Update(params: T.Update.Request): Promise<T.Update.Response> {
    try {
        const schema = Joi.object({
            username: Joi.string().required(),
            password: Joi.string(),
            fullName: Joi.string(),
            phone: Joi.number(),
            image: Joi.string().uri(),
            state: Joi.boolean()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function View(params: T.View.Request): Promise<T.View.Response> {
    try {
        const schema = Joi.object({
            limit: Joi.number(),
            offset: Joi.number(),
            state: Joi.boolean()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function FindOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {
    try {
        const schema = Joi.object({
            id: Joi.number(),
            username: Joi.string()
        }).xor('id', 'username');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}

export async function Count(params: T.Count.Request): Promise<T.Count.Response> {
    try {
        const schema = Joi.object({
            fullName: Joi.string(),
            state: Joi.boolean()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
